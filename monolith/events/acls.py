import requests
from conference_go.keys import PEXEL_KEY, WEATHER_PEXEL_KEY
import json

def getCityPhoto(city):
        response = requests.get(url=f'https://api.pexels.com/v1/search?query={city}', headers={"Authorization": PEXEL_KEY})
        data = json.loads(response.content)
        return data["photos"][0]["src"]["original"]


def get_weather_data(city, state):
        # translate city/state strings into lat/lon numbers
        # this info comes from geocoding-api from openweather.org
        params = {
                "q": f"{city},{state},US",
                "limit": 1,
                "appid": WEATHER_PEXEL_KEY,
        }
        # this url is from the API call from https://openweathermap.org/api/geocoding-api
        url = "http://api.openweathermap.org/geo/1.0/direct"
        response = requests.get(url, params=params)
        print(city)
        print(state)
        content = response.json()
        print(content)
        try:
            latitude = content[0]["lat"]
            longitude = content[0]["lon"]
        except (KeyError, IndexError):
               print("return A")
               return None
        # print("Latitude: " + str(latitude) + ", Longitude: " + str(longitude))
        #this url comes from the API call for current weather data from https://openweathermap.org/current
        weather_url = "https://api.openweathermap.org/data/2.5/weather"

        weather_params = {
               "lat":latitude,
               "lon": longitude,
               "appid": WEATHER_PEXEL_KEY,
               "units": "imperial",
        }
        response = requests.get(weather_url, params=weather_params)
        content = response.json()

        try:
            description = content["weather"][0]["description"]
            temp = content["main"]["temp"]
        except (KeyError, IndexError):
              print("return B")
              return None
        return {"description": description, "temp":temp}
