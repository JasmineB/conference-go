from django.http import JsonResponse
from .models import Conference, Location
# from common.json import ModelEncoder
from .encoders import ConferenceDetailEncoder, LocationDetailEncoder, LocationListEncoder, ConferenceVOListEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import State
import requests
from .acls import getCityPhoto, get_weather_data
# class LocationListEncoder(ModelEncoder):
#     model = Location
#     properties = ["name"]

# class LocationDetailEncoder(ModelEncoder):
#     model = Location
#     properties = [
#         "name",
#         "city",
#         "room_count",
#         "created",
#         "updated",
#         # "state"
# # in json.py there is a def get_extra_data(self, o): return {}, can help work with forien keys,
# # o is the specific thing you are looking at like specific location
#     ]
#     def get_extra_data(self, o):
#         return {"state": o.state.abbreviation}

# class ConferenceListEncoder(ModelEncoder):
#     model = Conference
#     # automatically adds href
#     properties = ["name"]

# class ConferenceDetailEncoder(ModelEncoder):
#     model = Conference
#     properties = [
#         "name",
#         "starts",
#         "ends",
#         "description",
#         "created",
#         "updated",
#         "max_presentations",
#         "location",

#     ]
#     encoders = {"location": LocationListEncoder()}


@require_http_methods(["GET","POST"])
def api_list_conferences(request):
    if request.method == "GET":
    # """
    # Lists the conference names and the link to the conference.

    # Returns a dictionary with a single key "conferences" which
    # is a list of conference names and URLS. Each entry in the list
    # is a dictionary that contains the name of the conference and
    # the link to the conference's information.

    # {
    #     "conferences": [
    #         {
    #             "name": conference's name,
    #             "href": URL to the conference,
    #         },
    #         ...
    #     ]
    # }
    # """
    # response = []
        conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
        return JsonResponse({"conferences": conferences}, encoder=ConferenceVOListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
            conference = Conference.objects.create(**content)
            return JsonResponse(
                conference,
                encoder=ConferenceDetailEncoder,
                safe=False,
            )
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    if request.method == "GET":
    # """
    # Returns the details for the Conference model specified
    # by the id parameter.

    # This should return a dictionary with the name, starts,
    # ends, description, created, updated, max_presentations,
    # max_attendees, and a dictionary for the location containing
    # its name and href.

    # {
    #     "name": the conference's name,
    #     "starts": the date/time when the conference starts,
    #     "ends": the date/time when the conference ends,
    #     "description": the description of the conference,
    #     "created": the date/time when the record was created,
    #     "updated": the date/time when the record was updated,
    #     "max_presentations": the maximum number of presentations,
    #     "max_attendees": the maximum number of attendees,
    #     "location": {
    #         "name": the name of the location,
    #         "href": the URL for the location,
    #     }
    # }
    # """
        conference = Conference.objects.get(id=id)
        ############## ADD WEATER DATA
        # weather = {"description": "Bad", "temperature": 55}
        weather = get_weather_data(conference.location.city, conference.location.state)
        print(weather)
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )
        return JsonResponse({"conference": conference, "weather": weather}, encoder=ConferenceDetailEncoder, safe=False )
# safe = False will remove requirement that this needs to be a dictionary in Insomnia
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
                    conference,
                    encoder=ConferenceDetailEncoder,
                    safe=False,
        )











@require_http_methods(["GET","POST"])
def api_list_locations(request):
    if request.method == "GET":
    # """
    # Lists the location names and the link to the location.

    # Returns a dictionary with a single key "locations" which
    # is a list of location names and URLS. Each entry in the list
    # is a dictionary that contains the name of the location and
    # the link to the location's information.

    # {
    #     "locations": [
    #         {
    #             "name": location's name,
    #             "href": URL to the location,
    #         },
    #         ...
    #     ]
    # }
    # """
    # response = []
        locations = Location.objects.all()
    # for location in locations:
    #     response.append({
    #         "name": location.name,
    #         "href":location.get_api_url()

    #     })
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder)
# change response to locations?
    else:

        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"]=state

                    #####To add a photo
        # content['image_url'] = getCityPhoto(content['city'])
            photo = getCityPhoto(content['city'])
            content['image_url'] = photo

        # response = requests.get(url=f'https://api.pexels.com/v1/search?query={content["city"]}', headers={"Authorisation": "xZMNiCA4CH5woyk2cQuVtTAjt6EO0xYpfxDi5mosRo1ok8jhnbCAJfqQ"})
        # print(response.json()["photos"][0]["src"]["original"])

            location = Location.objects.create(**content)
            return JsonResponse(
            # "name": location.name,
            # "href": location.get_api.url(),
                location,
                encoder = LocationDetailEncoder,
                safe=False,
            )
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    if request.method == "GET":
    # """
    # Returns the details for the Location model specified
    # by the id parameter.

    # This should return a dictionary with the name, city,
    # room count, created, updated, and state abbreviation.

    # {
    #     "name": location's name,
    #     "city": location's city,
    #     "room_count": the number of rooms available,
    #     "created": the date/time when the record was created,
    #     "updated": the date/time when the record was updated,
    #     "state": the two-letter abbreviation for the state,
    # }
    # """
        location = Location.objects.get(id=id)
    # return JsonResponse({
    #     "name":location.name,
    #     "city":location.city,
    #     "room_count":location.room_count,
    #     "created":location.created,
    #     "updated":location.updated,
    #     "state":
    #         location.state.abbreviation,

    # })
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
            # copied from create
        content = json.loads(request.body)
        try:
        # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

    # new code
        Location.objects.filter(id=id).update(**content)

    # copied from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
