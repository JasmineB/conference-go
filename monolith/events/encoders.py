from common.json import ModelEncoder
from events.models import Location, Conference


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "image_url",
        "created",
        "updated",
        # "state"
# in json.py there is a def get_extra_data(self, o): return {}, can help work with forien keys,
# o is the specific thing you are looking at like specific location
    ]
    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}

class ConferenceVOListEncoder(ModelEncoder):
    model = Conference
    # automatically adds href
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "location",

    ]
    encoders = {"location": LocationListEncoder()}
