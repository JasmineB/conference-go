from common.json import ModelEncoder
from attendees.models import Attendee, ConferenceVO, AccountVO



class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
        # "conference": {
        #     "name":attendee.conference.name,
        #     "href":attendee.conference.get_api_url(),
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder()
    }
    def get_extra_data(self, o):
        count = len(AccountVO.objects.filter(email=o.email))
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}
        # return {"conference": o.conference.name}
